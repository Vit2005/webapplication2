﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nemiro.OAuth;
using Nemiro.OAuth.Clients;
using System.Text;
using WebApplication2.Models;
using System.Collections.Specialized;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ExternalLogin(string provider)
        {
            // формируем обратный адрес (поддерживают не все поставщики)
            string returnUrl = Url.Action("ExternalLoginResult", "Home", null, null, Request.Url.Host);
            // перенаправляем пользователя на авторизацию
            NameValueCollection scope = new NameValueCollection();
            scope.Add("scope", "VALUABLE_ACCESS");
            return Redirect(OAuthWeb.GetAuthorizationUrl(provider, scope, returnUrl));
        }

        public ActionResult ExternalLoginResult()
        {
            string output = "";
            var result = OAuthWeb.VerifyAuthorization();
            output += String.Format("Provider:  {0}\r\n", result.ProviderName);
            
            if (result.IsSuccessfully)
            {
                // успешно авторизованный
                var user = result.UserInfo;
                var token = result.AccessTokenValue;
                OdnoklassnikiModel.AccessToken = result.AccessTokenValue;
                Random r = new Random();
                //string req = "api/friends/get?application_key=CBAGBNCEEBABABABA&sig=" + OdnoklassnikiModel.Instatse.md5string() +
                //    "&session_key=" + OdnoklassnikiModel.AccessToken;
                string call_id = r.Next(1000000000).ToString();
                string transaction_id = r.Next(1000000000).ToString();
                string transaction_time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");


                //string data = "amount=1" +
                //    "&application_key=" + OdnoklassnikiModel.ApplicationKey +
                //    "&call_id=" + call_id +
                //    "&method=callbacks.payment" +
                //    "&product_code=IDDQD" +
                //    "&session_key=null" +
                //    "&transaction_id=" + transaction_id +
                //    "&transaction_time=" + transaction_time +
                //    "&uid=" + user.UserId;

                //string req = "fb.do?method=callbacks.payment" +
                //    "&application_key=" + OdnoklassnikiModel.ApplicationKey +
                //    "&call_id=" + call_id +
                //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data) +
                //    "&uid=" + user.UserId +
                //    "&amount=1" +
                //    "&transaction_time=" + transaction_time + //2010-02-23 19:06:55
                //    "&product_code=IDDQD&transaction_id=" + transaction_id +
                //    "&access_token=" + OdnoklassnikiModel.AccessToken;
                string sigSecret = OdnoklassnikiModel.Instatse.md5string(OdnoklassnikiModel.AccessToken);


                //string req = "api/callbacks/payment?" +
                //    data +
                //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data.Replace("&", "") + sigSecret, "") +
                //    "&access_token=" + OdnoklassnikiModel.AccessToken;
                //string req = "fb.do?" +
                //    data +
                //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data.Replace("&", "") + sigSecret, "") +
                //"&access_token=" + OdnoklassnikiModel.AccessToken;

                //string req = "api/payment/getUserAccountBalance?application_key=" + OdnoklassnikiModel.ApplicationKey +
                //    "&sig=" + "123" +
                //    "&access_token=" + OdnoklassnikiModel.AccessToken;
                //string req = "fb.do?method=payment.getUserAccountBalance&application_key=" + OdnoklassnikiModel.ApplicationKey +
                //    "&sig=" + "123" +
                //    "&access_token=" + OdnoklassnikiModel.AccessToken;

                //OdnoklassnikiModel.Instatse.SendData(
                //    "fb.do?method=callbacks.payment" +
                //    "&application_key=" + OdnoklassnikiModel.ApplicationKey + 
                //    "&call_id=" + r.Next(1000000000)+
                //    "&sig=" + OdnoklassnikiModel.Instatse.md5string() +
                //    "&uid=" + user.UserId + 
                //    "&amount=1" +
                //    "&transaction_time=" +
                //    DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + //2010-02-23 19:06:55
                //    "&product_code=IDDQD&transaction_id="+r.Next(1000000000));
                //string resp = OdnoklassnikiModel.Instatse.SendData(
                //    "api/payment/getUserAccountBalance?application_key=CBAGBNCEEBABABABA&sig=" + OdnoklassnikiModel.Instatse.md5string(OdnoklassnikiModel.AccessToken)+
                //    "&access_token=" + OdnoklassnikiModel.AccessToken);

                //string data = "application_key=CBAGBNCEEBABABABA";
                //string sigSecret = OdnoklassnikiModel.Instatse.md5string(OdnoklassnikiModel.AccessToken, "9DE653024015EEAC28F48FE7");
                //string req = "api/friends/get?application_key=CBAGBNCEEBABABABA&access_token=" + OdnoklassnikiModel.AccessToken +
                //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data + sigSecret);

                string data = "application_key=CBAGBNCEEBABABABA&fields=uid,first_name,last_name&uids=254876682293";
                string req = "api/users/getInfo?application_key=CBAGBNCEEBABABABA" +
                    "&fields=uid,first_name,last_name&uids=254876682293" +
                    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data.Replace("&", "") + sigSecret, "") +
                    "&access_token=" + OdnoklassnikiModel.AccessToken;

                string resp = OdnoklassnikiModel.Instatse.SendData(req);

                output += String.Format("User ID:   {0}\r\n", user.UserId);
                output += String.Format("Name:      {0}\r\n", user.DisplayName);
                output += String.Format("Email:     {0}\r\n", user.Email);
                output += String.Format("Token:     {0}\r\n", token);
                output += String.Format("Request:   {0}\r\n", req);
                //output += String.Format("Signature: {0}\r\n", data);
                output += String.Format("Response:  {0}\r\n", resp);
            }
            else
            {
                // ошибка
                output += result.ErrorInfo.Message;
            }
            return new ContentResult
            {
                Content = output,
                ContentType = "text/plain"
            };
        }

        
    }
}