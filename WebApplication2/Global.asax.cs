﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Nemiro.OAuth;
using Nemiro.OAuth.Clients;
using WebApplication2.Models;

namespace WebApplication2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            OAuthManager.RegisterClient(new OdnoklassnikiClient("1126962432", "9DE653024015EEAC28F48FE7", "CBAGBNCEEBABABABA") { Scope = "VALUABLE_ACCESS" });
            //OAuthManager.RegisterClient("Odnoklassniki", "CBAGBNCEEBABABABA", "9DE653024015EEAC28F48FE7", "VALUABLE_ACCESS");
            OdnoklassnikiModel empty = new OdnoklassnikiModel();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
