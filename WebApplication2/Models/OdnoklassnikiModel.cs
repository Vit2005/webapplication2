﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebApplication2.Models
{
    public class OdnoklassnikiModel
    {
        public static string AccessToken;
        public static string ApplicationKey = "CBAGBNCEEBABABABA";
        public const string PrivateKey = "9DE653024015EEAC28F48FE7";
        private static OdnoklassnikiModel _instatse;

        public static OdnoklassnikiModel Instatse
        {
            get { return _instatse; }
        }

        public OdnoklassnikiModel()
        {
            _instatse = this;
        }


        public static string GetMD5Hash(string input)
        {
            var x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            var s = new StringBuilder();
            foreach (var b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }

        public string md5string(string data, string privateKey = PrivateKey)
        {
            return GetMD5Hash(string.Format("{0}{1}", data, privateKey));
        }

        public static HttpWebResponse PostMethod(string postedData, string postUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postUrl);
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;

            UTF8Encoding encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(postedData);

            request.ContentType = "application/xml";//   application/x-www-form-urlencoded
            request.ContentLength = bytes.Length;

            //using (var newStream = request.GetRequestStream())
            //{
            //    newStream.Write(bytes, 0, bytes.Length);
            //    newStream.Close();
            //}
            return (HttpWebResponse)request.GetResponse();
        }

        public string SendData(string postedData)
        {
            var response = PostMethod("", "http://api.ok.ru/" + postedData); //http://api.odnoklassniki.ru/
            if (response != null)
            {
                var strreader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                var responseToString = strreader.ReadToEnd();
                return responseToString.ToString();
            }
            return "";
        }
    }
}